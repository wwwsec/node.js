'use strict';

var restify = require('restify')
  , db      = require('./db');

var server = restify.createServer({
  name: 'json-server'
});

server.use(restify.bodyParser());
server.use(restify.queryParser());

server.get('/get/:tb/:index/:size/:f/:v', getTranslations); // curl -i http://localhost:8080/get
// server.post('/create', createTranslation); // curl -i -X POST -H 'Content-Type: application/json' -d  "userId=1&dictionary=1&originalTranslationId=0&fromWord=Fisk&fromDescription=&toWord=Zivis&toDescription=" localhost:8080/create
// server.put('/update', updateTranslation); // curl -i -X PUT -H 'Content-Type: application/json' -d  "userId=1&dictionary=1&originalTranslationId=0&fromWord=Fisk&fromDescription=&toWord=Zivis&toDescription=" localhost:8080/update
// server.del('/delete', deleteTranslation); // curl -i -X DELETE http://localhost:8080/delete/51374299e669481c48a25c8c

server.listen(8080, function() {
  console.log('%s listening at %s', server.name, server.url);
});

function callback (err, result) {
  return err ? res.send(err) : res.json(result);
}

function getTranslations(req, res) {
  var fields = [
    req.params.tb, 
    req.params.index,
    req.params.size,
    req.params.f,
    req.params.v
  ];
  console.log(fields);
  db.getTranslationsByDate(fields, function (err, result) {   return err ? res.send(err) : res.json(result); });
  console.log(fields);
}
